# Personata #

It is an app created for you to view and evaluate your facebook usage. You could know your monthly comments, likes and posts, view your friends as well as share and invite them to use the app.

### Website Link ###
[Personata](http://54.254.165.1/dev/)

### Team Members ###

* Bathini Yatish - yatishby@gmail.com - A0091545A
* Tang Ning - a0105529@ nus.edu.sg - A0105529H
* Xia Yiping - eva.ping2506@gmail.com - A0105670M
* Zhang Yichuan - a0105518@ nus.edu.sg - A0105518L

### Contributions ###

* Yatish: Backend Data Retrival & Database Operation
* Tang Ning: UI and Backend Integration & Events Data
* Zhang Yichuan: HighCharts API & Monthly Likes, Comments & Cache Data
* Xia Yiping: UI design & UI Integration